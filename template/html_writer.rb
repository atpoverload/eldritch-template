require_relative './card.rb'

def write_card_sheet(cards)
  html = ""

  html << <<-HEADER
<!doctype html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
  <link rel="stylesheet" type="text/css" href="../card.css">
</head>

<body>
  HEADER

  cards.each do |card|
    html << card.toHTML
  end

  html << <<-FOOTER
</body>

</html>
  FOOTER

  html
end
