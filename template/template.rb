require_relative './parser.rb'
require_relative './html_writer.rb'

filename = ARGV[0].chomp(".card").concat(".html").sub!("card", "sheet");

cards = File.open(filename, "w")

cards.write(write_card_sheet(parse_file(ARGV[0])))
