require_relative './card.rb'

def parse_card(lines)
  name, cost = lines[0].split('[')
  name.strip!
  if(!cost.nil?)
    cost.chomp!(']')
  end
  type = lines[1].chomp

  text = []
  stop = -1
  if type != "Character" then
    attack, health = ""
  else
    attack, health = lines[-1].split('/')
    stop = lines.size - 2
  end

  lines[2..stop].each do |line|
    text.push(line)
  end

  build(name, type, cost, text, attack, health)
end

def parse_block(file)
  lines = []
  line = file.readline
  while(line != "\n" && line != "\r\n")
    if line.include? "\r" then
      lines.push(line.chomp.chomp)
    else
      lines.push(line.chomp)
    end
    line = file.readline
  end
  lines
end

def parse_divider(file)
  while(!file.eof?)
    line = file.readline
    if line == "\n" || line == "\r\n" then
      break
    end
  end
end

def parse_file(filename)
  file = File.open(filename, "r")

  cards = []
  while(!file.eof?)
    parse_divider(file)
    if(!file.eof?)
      cards.push(parse_card(parse_block(file)))
    end
  end
  cards
end
