require 'json'

def build(name, type, cost, text, attack, health)
  case type
  when "Place"
    Place.new(name, text, cost)
  when "Spell", "Ritual"
    Entity.new(name, cost, text, type)
  when "Character"
    Character.new(name, cost, text, attack, health)
  end
end

class Card
  attr_reader :name
  attr_reader :text
  attr_reader :type
  def initialize(name, text)
    @name = name
    @text = text
    @type = "None"
  end
  def toJSON
    hash = {
      "name" => @name,
      "type" => @type,
      "text" => @text
    }
    JSON.pretty_generate(hash)
  end
  def toHTML
    html = ""
    html << "  <div class=\"card\">\n"
    html << "    <div class=\"name\">#{@name}</div>\n"

    html << "    <div class=\"image\"></div>\n"

    html << "    <div class=\"info\">\n"
    html << "      <div class=\"type\">#{@type}</div>\n"
    html << "    </div>\n"

    html << "    <div class=\"text\">\n"
    @text.each do |line|
      html << "      <div class=\"line\">#{line}</div>\n"
    end
    html << "    </div>\n"

    html << "  </div>\n\n"

    html
  end
end

class Place < Card
  attr_reader :mana
  def initialize(name, text, mana)
    super(name, text)
    @mana = mana
    @type = "Place"
  end
  def toJSON
    hash = {
      "name" => @name,
      "type" => @type,
      "text" => @text,
      "mana" => @mana
    }
    JSON.pretty_generate(hash)
  end
  def toHTML
    html = ""
    html << "  <div class=\"card\">\n"
    html << "    <div class=\"cost\"><br/></div>\n"
    html << "    <div class=\"name\">#{@name}</div>\n"

    html << "    <div class=\"image\"></div>\n"

    html << "    <div class=\"info\">\n"
    html << "      <div class=\"type\">#{@type}</div>\n"
    html << "      <div class=\"mana\">#{@mana}</div>\n"
    html << "    </div>\n"

    html << "    <div class=\"text\">\n"
    @text.each do |line|
      html << "      <div class=\"line\">#{line}</div>\n"
    end
    html << "    </div>\n"

    html << "  </div>\n\n"

    html
  end
end

class Entity < Card
  attr_reader :cost
  def initialize(name, cost, text, type)
    super(name, text)
    @cost = cost
    @type = type
  end
  def toJSON
    hash = {
      "name" => @name,
      "cost" => @cost,
      "type" => @type,
      "text" => @text
    }
    JSON.pretty_generate(hash)
  end
  def toHTML
    html = ""
    html << "  <div class=\"card\">\n"
    if cost.empty?
      html << "    <div class=\"cost\"><br/></div>\n"
    else
      html << "    <div class=\"cost\">#{@cost}</div>\n"
    end
    html << "    <div class=\"name\">#{@name}</div>\n"

    html << "    <div class=\"image\"></div>\n"

    html << "    <div class=\"info\">\n"
    html << "      <div class=\"type\">#{@type}</div>\n"
    html << "    </div>\n"

    html << "    <div class=\"text\">\n"
    @text.each do |line|
      html << "      <div class=\"line\">#{line}</div>\n"
    end
    html << "    </div>\n"

    html << "  </div>\n\n"

    html
  end
end

class Character < Entity
  attr_reader :attack
  attr_reader :health
  def initialize(name, cost, text, attack, health)
    super(name, cost, text, "Character")
    @attack = attack
    @health = health
  end
  def toJSON
    hash = {
      "name" => @name,
      "cost" => @cost,
      "type" => @type,
      "text" => @text,
      "stats" => {"attack" => @attack, "health" => @health }
    }
    JSON.pretty_generate(hash)
  end
  def toHTML
    html = ""
    html << "  <div class=\"card\">\n"
    if cost.empty?
      html << "    <div class=\"cost\"><br/></div>\n"
    else
      html << "    <div class=\"cost\">#{@cost}</div>\n"
    end
    html << "    <div class=\"name\">#{@name}</div>\n"

    html << "    <div class=\"image\"></div>\n"

    html << "    <div class=\"info\">\n"
    html << "      <div class=\"type\">#{@type}</div>\n"
    html << "      <div class=\"attack\">#{@attack}</div>\n"
    html << "      <div class=\"divider\">/</div>\n"
    html << "      <div class=\"health\">#{@health}</div>\n"
    html << "    </div>\n"

    html << "    <div class=\"text\">\n"
    @text.each do |line|
      html << "      <div class=\"line\">#{line}</div>\n"
    end
    html << "    </div>\n"

    html << "  </div>\n\n"

    html
  end
end
