# README #

This project only requires the use of Ruby to run the script.

### Eldritch Template ###

* A set of ruby scripts to parser newline delimited text and create a card from it according to a template.

* The example material included is for a conceptual card game called Eldritch inspired by Magic: the Gathering's Legacy and Vintage formats.

* Version 0.1

### How do I run the template? ###

* To run this, you pass as a command line argument the name of the filename you wish to parse, e.g.:

ruby parser.rb [FILENAME]
